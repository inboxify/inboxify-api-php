#!/bin/bash

PHP_VERSION=$(php -v | head -n 1 | cut -d " " -f 2 | cut -c 1-3)
echo "Building for PHP $PHP_VERSION"
cd /var/www/html/


# phpcpd
echo "phpcpd"
if [[ "5.6" = $PHP_VERSION ]]
then
    phpcpd3 src/
else
    phpcpd src/
fi


# phpcs
echo "phpcs"
phpcs --standard=PSR2 src/Inboxify/Api/*.php src/Inboxify/Api/Client/*.php


# phpdoc
echo "phpdoc"
cp build/phpdoc/phpdoc.xml.dist build/phpdoc/phpdoc.xml

if [[ "5.6" = $PHP_VERSION ]]
then
    phpdoc2 -c build/phpdoc/phpdoc.xml --force
else
    phpdoc -c build/phpdoc/phpdoc.xml --force
fi

rm build/phpdoc/phpdoc.xml


# phpmd
echo "phpmd"
phpmd src/Inboxify/Api/Cache.php,src/Inboxify/Api/Client/Http.php,src/Inboxify/Api/Client/RestJson.php,src/Inboxify/Api/Client.php,src/Inboxify/Api/Config.php text cleancode,codesize,controversial,design,naming,unusedcode


# phpunit
echo "phpunit"
cp build/phpunit/config.env.php.dist build/phpunit/config.php

if [[ "8.1" = $PHP_VERSION ]]
then
    cp build/phpunit/phpunit.xml.dist build/phpunit/phpunit.xml
    phpunit -c build/phpunit/phpunit.xml
else
    cp build/phpunit/phpunit5.xml.dist build/phpunit/phpunit.xml
    phpunit5 -c build/phpunit/phpunit.xml
fi

rm build/phpunit/phpunit.xml build/phpunit/config.php


# phar
echo "phar"
phar pack -f build/phar/inboxify-api-php.phar src/AutoloaderPsr4.php src/autoload.php src/Inboxify/Api/*.php src/Inboxify/Api/Client/*.php
