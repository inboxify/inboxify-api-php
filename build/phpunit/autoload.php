<?php

/**
 * Inboxify PHP API (https://www.inboxify.eu/index.php)
 *
 * @author Inboxify <info@inboxify.eu>
 * @copyright (c) 2016 - 2017 - Inboxify
 * @license https://gitlab.com/inboxify/inboxify-php-api/blob/master/LICENSE.txt
 * @package Inboxify\API\Client
 * @version 1.0.2
 */

error_reporting(E_ALL);
ini_set('display_errors', true);

require_once __DIR__ . '/AutoloaderPsr4.php';

define('IY_ROOT', realpath(__DIR__ . '/../../') . '/');

$loader = new AutoloaderPsr4;
$loader->register();
$loader->addNamespace('Inboxify\Api', IY_ROOT . 'src/Inboxify/Api');
