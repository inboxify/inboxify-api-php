# PHP Client for Inboxify API

## Requirements

- PHP >= 5.6 - 8.x
- [PHP module HASH](http://php.net/manual/en/book.hash.php)
- [PHP module JSON](http://php.net/manual/en/book.json.php)
- [PHP module Sockets](http://php.net/manual/en/book.sockets.php)
- [Inboxify API Help](https://api.inboxify.nl/help)
- [Inboxify API Account](https://www.inboxify.nl/)

## Installation

### Composer

Client can be easily installed using [Composer](https://getcomposer.org/). The Package name is: `inboxify/inboxify-php-api`.

Installation using composer: `composer require "inboxify/inboxify-php-api:1.0.*"`

Example of `composer.json`:

    {
        "require": {
            "inboxify/inboxify-php-api": "1.0.*"
        }
    }

### Single file distribution

You can find single file distribution with concatenated classes in `build/dist/inboxify-php-api.php` file, 
which you can include to your project easily without using composer or autoloading.

### Caching

If you want to use file cache, please make sure cache directory is writable by user of the code / web server.
Also don't forget cache directory MUST NOT be accessible by web users.

## Configuration

Configuration is associative array of configuration directives as follows:

    $config = array(
        // enable or disable cache
        'cache' => true,
        // cache directory (must be writeable, must end with /)
        'cacheDir' => '/tmp/',
        // api endpoint, please do not change this value, unless instructed to
        'endPoint' => 'https://api.inboxify.nl/',
        // your API key
        'key' => 'INSERTAPIKEY',
        // name of the inboxify list you want to use as a default
        'list' => 'INSERTLISTNAME',
        // your API secret
        'secret' => 'INSERTAPISECRET',
        // advanced: socket timeout
        'timeOutSocket' => 10,
        // advanced: socket stream read timeout
        'timeOutStream' => 10,
        // cache time to live in seconds (3600 sec = 1 hour)
        'ttl' => 3600,
    );

## Running

Install the client by Composer or include/require it as single file distribution version. 
Use the configuration example, and set following required keys:

- `key`: API key
- `secret`: API secret
- `list`: Default list ID or Name

Then pass the configuration to the client:

    <?php

    use Inboxify\Api\Client;

    try {
      $config = array(/* See configuration example */);
      $client = Client::getInstance($config);

      print $client->isConnected() ? 'Huray!' : 'Yay...';
    } catch (\Throwable $e) {
      print 'Something went wrong: ' . $e->getMessage();
    }

### Examples

You can find more examples covering most of the use cases in: `build/examples/example.php`.

## Files Overview

- `build/`: Build Files
- `build/dist/`: Single File Distribution
- `build/docs/`: PHPDoc related Files
- `build/examples/`: Additional Examples
- `build/tests/`: PHPUnit related Files
- `src/`: all PHP Classes
- `src/Inboxify/Api/`: all PHP API Client Classes
- `src/Inboxify/Test/`: all PHPUnit Test Classes
- `.gitignore`: Gitignore File
- `LICENSE`: Full Text of MIT License
- `README.md`: This File
- `composer.json`: Composer Package Definition

## Copyright

MIT License

## Contact Information

- Email: [info@inboxify.nl](mailto:info@inboxify.nl)
- Website: [Inboxify](https://www.inboxify.nl/)

## Changelog

- `1.0.4` (2021-12-24):
    - changed PHP compatibility to >= 5.6 - 8.x

- `1.0.4` (2018-07-15):
    - added chunked transfer encoding support to http client to make it standard compliant
    - added gzip support to http client
    - changed single file distribution to be phar file
    - ci/cd with php qa (docker + php qa tools + gitlab-ci)
    - config doesn't require endpoint to end with /

- `1.0.2` (2016-06-30):
    - Rebuild single file distribution only

- `1.0.1` (2016-06-30):
    - Changed API end-point, emails, URLs from inboxify.eu to inboxify.nl
    - Fixed minor README.md formatting issues

- `1.0.0` (2016-12-19):
    - Initial Version
