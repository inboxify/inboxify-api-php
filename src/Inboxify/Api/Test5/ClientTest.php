<?php

/**
 * Inboxify PHP API (https://www.inboxify.nl/index.php)
 *
 * @author    Inboxify <info@inboxify.nl>
 * @copyright (c) 2016 - 2022 - Inboxify
 * @license   https://gitlab.com/inboxify/inboxify-php-api/blob/master/LICENSE.txt
 * @package   Inboxify\API\Client
 * @version   1.0.4
 */

/**
 * Namespace
 *
 * @package Inboxify\Api\Test5
 */
namespace Inboxify\Api\Test5;

use Inboxify\Api\Cache;
use Inboxify\Api\Client;
use Inboxify\Api\Config;
use PHPUnit\Framework\TestCase;

class ClientTest extends TestCase
{
    const CLS_CLIENT = 'Inboxify\Api\Client';
    const EMAIL = 'e@ma.il';
    const EMAIL2 = 'e2@ma.il';
    const EMAIL_BAD = 'e3@ma.il';
    const TAG1 = 'Tag 1';
    const TAG2 = 'Tag 2';
    const TAG_BAD = 'Tag 3';
    
    protected $cache;
    protected $config;
    protected $configData;
    protected $configFile;
    protected $contactData = [
        'email'             => self::EMAIL,
        'companyName'       => 'company',
        'firstName'         => 'first',
        'insertion'         => 'mid',
        'lastName'          => 'last',
        'sex'               => 1,
        'telephone'         => '123123123',
        'mobile'            => '321321321',
        'address'           => 'address 1',
        'postalCode'        => '12345',
        'city'              => 'city',
        'countryCode'       => 'NLD',
        'free1'             => 'free1',
        'free2'             => 'free2',
        'free3'             => 'free3',
        'remarks'           => 'remark, remark',
        // 'tags'              => ['tag1', 'tag2'], // INFO issue in later test
        'customDate'        => '2018-12-31T00:00:00'
    ];
    
    public function setUp()
    {
        $this->configFile = IY_ROOT . 'build/phpunit/config.php';
        
        if (!is_readable($this->configFile)) {
            throw new \Exception('Example config file not found.');
        }
        
        $this->configData = include $this->configFile;
        
        if (!is_array($this->configData) || !count($this->configData)) {
            throw new \Exception('Example configuration is empty.');
        }
        
        $this->config = new Config($this->configData);
        $this->cache = new Cache($this->config);
        $this->cache->purge();
        $this->client = new Client($this->cache, $this->config);
        
        // INFO optionally enable display request / response headers
        $this->client->getRestJson()->getHttp()->setPrintRequestResponse(true);
    }
    
    protected function helperBulkPostDeleteContacts()
    {
        $contacts = $this->client->getContacts();

        if (count($contacts)) {
            foreach ($contacts as $contact) {
                $rs = $this->client->deleteContact($contact->email);
                $this->assertTrue($rs);
            }
        } else {
            $this->assertTrue(true); // must do a test
        }
    }
    
    public function testBulkPostDeleteContactsBefore()
    {
        $this->helperBulkPostDeleteContacts();
    }
    
    /**
     * @depends testBulkPostDeleteContactsBefore
     */
    public function testClient()
    {
        $this->assertTrue(
            self::CLS_CLIENT == get_class($this->client)
        );
    }
    
    /**
     * @depends testClient
     * @expectedException BadMethodCallException
     */
    public function testStaticClientException()
    {
        Client::getInstance(
            array(
            // missing required keys
            )
        );
    }
    
    /**
     * @depends testStaticClientException
     */
    public function testStaticClient()
    {
        $configData = array(
            'cacheDir' => IY_ROOT . 'cache/',
            'key' => $this->config->getKey(),
            'list' => $this->config->getList(),
            'secret' => $this->config->getSecret(),
        );
        
        $this->assertTrue(
            self::CLS_CLIENT == get_class(Client::getInstance($configData))
        );
    }
    
    /**
     * @depends testStaticClient
     */
    public function testStaticClient2()
    {
        $this->assertTrue(
            self::CLS_CLIENT == get_class(Client::getInstance()) // return existing instance
        );
    }
    
    /**
     * @depends testClient
     */
    public function testGetLists()
    {
        $lists = $this->client->getLists();
        
        $this->assertTrue(is_array($lists));
        $this->assertTrue(count($lists) >= 1);
        
        foreach ($lists as $list) {
            $this->assertTrue(isset($list->id) && !empty($list->id));
            $this->assertTrue(isset($list->list) && !empty($list->list));
            $this->assertTrue(isset($list->primary) && ( $list->primary === false || $list->primary === true));
        }
    }
    
    /**
     * @ depends testGetLists
     */
    public function testGetEmptyContacts()
    {
        $contacts = $this->client->getContacts();
        
        $this->assertTrue(is_array($contacts));
        
        $zero = count($contacts) == 0;
        
        $this->assertTrue($zero);
    }
    
    /**
     * @depends testGetEmptyContacts
     */
    public function testGetNonExistentContact()
    {
        $contact = $this->client->getContact(self::EMAIL);
        
        $this->assertTrue(is_null($contact));
    }
    
    /**
     * @depends testGetNonExistentContact
     */
    public function testPostContact()
    {
        $contact = new \stdClass();
        $contact->email = self::EMAIL;

        foreach ($this->contactData as $k => $v) {
            $contact->{$k} = $v;
        }
        
        $newContact = $this->client->postContact($contact);

        $this->assertTrue(is_object($newContact));
        
        $this->assertTrue(isset($newContact->email) && $newContact->email == $contact->email);
        $this->assertTrue(isset($newContact->id) && !empty($newContact->id));
        $this->assertTrue(isset($newContact->list) && !empty($newContact->list));

        foreach ($this->contactData as $k => $v) {
            $this->assertTrue($v == $newContact->{$k});
        }
    }
    
    /**
     * @depends testPostContact
     */
    public function testGetContact()
    {
        $contact = $this->client->getContact(self::EMAIL);
        
        $this->assertTrue(is_object($contact));
        
        $this->assertTrue(isset($contact->email) && !empty($contact->email));
        $this->assertTrue(isset($contact->id) && !empty($contact->id));
    }
    
    /**
     * @depends testGetContact
     */
    public function testPutContact()
    {
        $testName = 'test';
        
        $contact = $this->client->getContact(self::EMAIL);
        
        $newContact = clone $contact;
        $newContact->firstName = $testName;
        
        // update
        $this->assertTrue(
            $this->client->putContact(self::EMAIL, $newContact, null)
        );
        
        $contact = $this->client->getContact(self::EMAIL);
        
        $this->assertTrue(is_object($contact));
        $this->assertTrue(isset($newContact->firstName));
        $this->assertTrue(isset($contact->firstName));
        
        // check if updated
        $this->assertTrue(
            $newContact->firstName == $contact->firstName
        );
        
        $newContact = clone $contact;
        $newContact->unsubscribed = true;
        
        // unsubscribe
        $this->assertTrue(
            $this->client->putContact(self::EMAIL, $newContact, null, false)
        );
        
        $contact = $this->client->getContact(self::EMAIL);
        
        // check if unsubscribed
        $this->assertTrue($contact->unsubscribed);
        
        $newContact->unsubscribed = false;
        
        // subscribe
        $this->assertTrue(
            $this->client->putContact(self::EMAIL, $newContact, null, true)
        );
        
        $contact = $this->client->getContact(self::EMAIL);
        
        // check if subscribed
        $this->assertFalse($contact->unsubscribed);
        
        $contact = new \stdClass();
        $contact->email = self::EMAIL2;
        $contact->firstName = 'name';
        
        $res = $this->client->putContact(self::EMAIL2, $contact, null);
        
        $this->assertTrue(is_null($res));
    }
    
    /**
     * @depends testPutContact
     */
    public function testTags()
    {
        $tagsApi = $this->client->getTags(self::EMAIL);

        $this->assertTrue(is_array($tagsApi));
        $this->assertTrue(count($tagsApi) == 0);
        
        $tags = array(self::TAG1, self::TAG2);
        
        $this->client->postTags(self::EMAIL, $tags);
        
        $tagsApi = $this->client->getTags(self::EMAIL);
        
        $this->assertTrue(is_array($tagsApi));
        $this->assertTrue(count($tagsApi) == 2);
        $this->assertTrue(in_array(self::TAG1, $tagsApi));
        $this->assertTrue(in_array(self::TAG2, $tagsApi));
        
        $res = $this->client->deleteTag(self::EMAIL, self::TAG1);
        $this->assertTrue($res);
        
        $tagsApi = $this->client->getTags(self::EMAIL);
        
        $this->assertTrue(is_array($tagsApi));
        $this->assertTrue(count($tagsApi) == 1);
        $this->assertTrue(!in_array(self::TAG1, $tagsApi));
        $this->assertTrue(in_array(self::TAG2, $tagsApi));
        
        $res = $this->client->deleteTag(self::EMAIL, self::TAG_BAD);
        
        $this->assertNull($res);
    }
    
    /**
     * @depends testTags
     */
    public function testDeleteContact()
    {
        $res = $this->client->deleteContact(self::EMAIL);
        $this->assertTrue($res);
        
        $contact = $this->client->getContact(self::EMAIL);
        $this->assertTrue(is_null($contact));
        
        $res = $this->client->deleteContact(self::EMAIL_BAD);
        $this->assertNull($res);
    }
    
    /**
     * @depends testDeleteContact
     * @expectedException LengthException
     */
    function testBulkLimit()
    {
        $toobig = Client::LIMIT + 1;
        $toobigArray = range(0, $toobig);
        
        $this->client->postContacts($toobigArray);
    }
    
    /**
     * @depends testBulkLimit
     * @expectedException LengthException
     */
    function testGetLimit()
    {
        $toobig = Client::LIMIT + 1;
        
        $this->client->getContacts(0, $toobig);
    }
    
    /**
     * @depends testGetLimit
     * @expectedException LengthException
     */
    function testDeleteLimit()
    {
        $toobig = Client::LIMIT + 1;
        $toobigArray = range(0, $toobig);
        
        $this->client->deleteContacts($toobigArray);
    }
    
    /**
     * @depends testDeleteLimit
     */
    public function testBulkPostContact()
    {
        $contact = new \stdClass();
        $contact->email = self::EMAIL;
        
        $contact2 = new \stdClass();
        $contact2->email = self::EMAIL2;
        
        $contacts = array($contact, $contact2);
        
        $res = $this->client->postContacts($contacts);
        
        $this->assertTrue($res);
        
        $newContacts = $this->client->getContacts();

        $this->assertTrue(is_array($newContacts));
        $this->assertTrue(count($newContacts) == 2); // LATER can't test this wit not empty list
        
        $res = $this->client->postContactsBulk($contacts, null, true);
      
        $this->assertTrue(is_array($res));
        $this->assertTrue(isset($res[0]));
        $this->assertTrue(isset($res[1]));
        $this->assertTrue(isset($res[2]));
        $this->assertTrue(isset($res[3]));
        
        $this->assertEquals(2, $res[0]);
        $this->assertEquals(0, $res[1]);
        $this->assertEquals(2, $res[2]);
        
        $this->assertTrue(is_object($res[3]));
        
        $this->assertTrue(isset($res[3]->success));
        $this->assertTrue(isset($res[3]->failure));
        
        $this->assertTrue(is_array($res[3]->success));
        $this->assertTrue(is_array($res[3]->failure));
        
        $this->assertEquals(2, count($res[3]->success));
        $this->assertEquals(0, count($res[3]->failure));
        
        $res = $this->client->postContactsBulk($contacts, null, false);
        
        $this->assertTrue(is_array($res));
        $this->assertTrue(isset($res[0]));
        $this->assertTrue(isset($res[1]));
        $this->assertTrue(isset($res[2]));
        $this->assertTrue(isset($res[3]));
        
        $this->assertEquals(2, $res[0]);
        $this->assertEquals(2, $res[1]);
        $this->assertEquals(0, $res[2]);
        
        $this->assertTrue(is_object($res[3]));
        
        $this->assertTrue(isset($res[3]->success));
        $this->assertTrue(isset($res[3]->failure));
        
        $this->assertTrue(is_array($res[3]->success));
        $this->assertTrue(is_array($res[3]->failure));
        
        $this->assertEquals(0, count($res[3]->success));
        $this->assertEquals(2, count($res[3]->failure));
    }
    
    /**
     * @depends testBulkPostContact
     */
    public function testBulkUnsubscribe()
    {
        $contact = new \stdClass();
        $contact->email = self::EMAIL;
        $contact->unsubscribed = false;

        // INFO must force resubscribe this contact, because it was unsubscribed already
        $this->assertTrue(
            $this->client->putContact($contact->email, $contact, null, true)
        );
        
        $contact2 = new \stdClass();
        $contact2->email = self::EMAIL2;
        
        $contacts = array($contact, $contact2);

        $res = $this->client->postContactsUnsubscribe($contacts);

        $this->assertTrue(is_array($res));
        $this->assertTrue(isset($res[0]));
        $this->assertTrue(isset($res[1]));
        $this->assertTrue(isset($res[2]));
        $this->assertTrue(isset($res[3]));
        
        $this->assertEquals(2, $res[0]); // count
        $this->assertEquals(1, $res[1]); // fail
        $this->assertEquals(1, $res[2]); // success
        
        $this->assertTrue(is_object($res[3]));
        
        $this->assertTrue(isset($res[3]->success));
        $this->assertTrue(isset($res[3]->failure));
        
        $this->assertTrue(is_array($res[3]->success));
        $this->assertTrue(is_array($res[3]->failure));
        
        $this->assertEquals(1, count($res[3]->success));
        $this->assertEquals(1, count($res[3]->failure));
    }
    
    /**
     * @depends testBulkUnsubscribe
     */
    public function testBulkPostDeleteContactsAfter()
    {
        $this->helperBulkPostDeleteContacts();
    }
}
