<?php

/**
 * Inboxify PHP API (https://www.inboxify.nl/index.php)
 *
 * @author    Inboxify <info@inboxify.nl>
 * @copyright (c) 2016 - 2022 - Inboxify
 * @license   https://gitlab.com/inboxify/inboxify-php-api/blob/master/LICENSE.txt
 * @package   Inboxify\API\Client
 * @version   1.0.4
 */

/**
 * Namespace
 *
 * @package Inboxify\Api\Test5
 */
namespace Inboxify\Api\Test5;

use Inboxify\Api\Cache;
use Inboxify\Api\Config;
use PHPUnit\Framework\TestCase;

class CacheTest extends TestCase
{
    const CLS_CACHE = 'Inboxify\Api\Cache';
    const KEY1 = 'key1';
    const VALUE1 = 'value1';
    const KEY2 = 'key2';
    const VALUE2 = 'value2';
    
    protected $config;
    protected $configData;
    protected $configFile;
    
    public function setUp()
    {
        $this->configFile = IY_ROOT . 'build/phpunit/config.php';
        
        if (!is_readable($this->configFile)) {
            throw new \Exception('Example config file not found.');
        }
        
        $this->configData = include $this->configFile;
        
        if (!is_array($this->configData) || !count($this->configData)) {
            throw new \Exception('Example configuration is empty.');
        }
        
        $this->config = new Config($this->configData);
        $this->cache = new Cache($this->config);
    }
    
    public function testHtaccess()
    {
        $this->assertTrue(
            is_file($this->config->getCacheDir() . '.htaccess')
        );
    }
    
    public function testClass()
    {
        $this->assertTrue(
            self::CLS_CACHE == get_class($this->cache)
        );
    }
    
    public function testSet()
    {
        $this->assertTrue(
            $this->cache->set(self::KEY1, self::VALUE1)
        );
    }
    
    public function testSerialization()
    {
        $arr = array(self::KEY1 => self::VALUE1, self::KEY2 => self::VALUE2);
        
        $this->assertTrue(
            $this->cache->set('array', $arr)
        );
        
        $this->assertEquals(
            $arr,
            $this->cache->get('array')
        );
    }
    
    public function testHas()
    {
        $this->assertTrue(
            $this->cache->has(self::KEY1)
        );
    }
    
    public function testGet()
    {
        $this->assertEquals(
            self::VALUE1,
            $this->cache->get(self::KEY1)
        );
    }
    
    public function testRemove()
    {
        $this->assertTrue(
            $this->cache->remove(self::KEY1)
        );
        
        $this->assertFalse(
            $this->cache->has(self::KEY1)
        );
    }
    
    public function testPurge()
    {
        $this->assertTrue(
            $this->cache->set(self::KEY1, self::VALUE1)
        );
        
        $this->assertTrue(
            $this->cache->set(self::KEY2, self::VALUE2)
        );
        
        $this->cache->purge();
        
        $this->assertFalse(
            $this->cache->has(self::KEY1)
        );
        
        $this->assertFalse(
            $this->cache->has(self::KEY2)
        );
    }
}
