<?php

/**
 * Inboxify PHP API (https://www.inboxify.nl/index.php)
 *
 * @author    Inboxify <info@inboxify.nl>
 * @copyright (c) 2016 - 2022 - Inboxify
 * @license   https://gitlab.com/inboxify/inboxify-php-api/blob/master/LICENSE.txt
 * @package   Inboxify\API\Client
 * @version   1.0.4
 */

/**
 * Namespace
 *
 * @package Inboxify\Api\Test5
 */
namespace Inboxify\Api\Test5;

use Inboxify\Api\Client\RestJson;
use Inboxify\Api\Config;
use PHPUnit\Framework\TestCase;

class RestJsonTest extends TestCase
{
    const CLS_RESTJSON = 'Inboxify\Api\Client\RestJson';
    
    protected $configData;
    protected $configFile;
    protected $restJson;
    
    public function setUp()
    {
        $this->configFile = IY_ROOT . 'build/phpunit/config.php';
        
        if (!is_readable($this->configFile)) {
            throw new \Exception('Example config file not found.');
        }
        
        $this->configData = include $this->configFile;
        
        if (!is_array($this->configData) || !count($this->configData)) {
            throw new \Exception('Example configuration is empty.');
        }
        
        $this->config = new Config($this->configData);
        $this->restJson = new RestJson($this->config);
    }
    
    public function testRestJson()
    {
        $this->assertTrue(
            self::CLS_RESTJSON == get_class($this->restJson)
        );
    }
}
