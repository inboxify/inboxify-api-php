<?php

/**
 * Inboxify PHP API (https://www.inboxify.nl/index.php)
 *
 * @author    Inboxify <info@inboxify.nl>
 * @copyright (c) 2016 - 2022 - Inboxify
 * @license   https://gitlab.com/inboxify/inboxify-php-api/blob/master/LICENSE.txt
 * @package   Inboxify\API\Client
 * @version   1.0.4
 */

/**
 * Namespace
 *
 * @package Inboxify\Api\Test5
 */
namespace Inboxify\Api\Test5;

use Inboxify\Api\Config;
use Inboxify\Api\Client\Http;
use PHPUnit\Framework\TestCase;

class HttpTest extends TestCase
{
    const CLS_HTTP = 'Inboxify\Api\Client\Http';
    
    protected $config;
    protected $configData;
    protected $configFile;
    protected $http;
    
    public function setUp()
    {
        $this->configFile = IY_ROOT . 'build/phpunit/config.php';
        
        if (!is_readable($this->configFile)) {
            throw new \Exception('Example config file not found.');
        }
        
        $this->configData = include $this->configFile;
        
        if (!is_array($this->configData) || !count($this->configData)) {
            throw new \Exception('Example configuration is empty.');
        }
        
        $this->config = new Config($this->configData);
        
        $this->http = new Http($this->config);
    }
    
    public function testHttp()
    {
        $this->assertTrue(
            self::CLS_HTTP == get_class($this->http)
        );
    }
    
    public function testGet()
    {
        $this->http->get('https://en.wikipedia.org/wiki/Main_Page');
        
        $this->assertEquals(Http::OK, $this->http->getResponseCode());
        $this->assertNotEmpty($this->http->getResponseBody());
        $this->assertNotEmpty($this->http->getResponseMessage());
    }
    
    /**
     * @expectedException RuntimeException
     */
    public function testNonExistantDomain()
    {
        $this->http->get('http://asdfjoweijfoweifjwe.com');
        
        $this->assertTrue(empty($this->http->getResponseBody()));
        $this->assertTrue(empty($this->http->getResponseCode()));
        $this->assertTrue(empty($this->http->getResponseMessage()));
    }
}
