<?php

/**
 * Inboxify PHP API (https://www.inboxify.nl/index.php)
 *
 * @author    Inboxify <info@inboxify.nl>
 * @copyright (c) 2016 - 2022 - Inboxify
 * @license   https://gitlab.com/inboxify/inboxify-php-api/blob/master/LICENSE.txt
 * @package   Inboxify\API\Client
 * @version   1.0.4
 */

/**
 * Namespace
 *
 * @package Inboxify\Api\Test
 */
namespace Inboxify\Api\Test;

use Inboxify\Api\Config;
use PHPUnit\Framework\TestCase;

class ConfigTest extends TestCase
{
    const CLS_CONFIG = 'Inboxify\Api\Config';
    
    protected $config;
    protected $configData;
    protected $configFile;
    
    public function setUp() : void
    {
        $this->configFile = IY_ROOT . 'build/phpunit/config.php';
        
        if (!is_readable($this->configFile)) {
            throw new \Exception('Example config file not found.');
        }
        
        $this->configData = include $this->configFile;
        
        if (!is_array($this->configData) || !count($this->configData)) {
            throw new \Exception('Example configuration is empty.');
        }
        
        $this->config = new Config($this->configData);
    }
    
    public function testConfigData()
    {
        $this->assertTrue(
            self::CLS_CONFIG == get_class($this->config)
        );
        
        foreach ($this->configData as $k => $v) {
            $getter = 'get' . ucfirst($k);
            
            $this->assertTrue(
                $v == $this->config->$getter()
            );
            
            $this->assertTrue(
                $v == $this->config->$k
            );
        }
    }
    
    public function testTrailingSlash()
    {
        $config = $this->config->getConfig();
        
        $config['cacheDir'] = '/tmp/inboxify';
        $config['endPoint'] = 'https://api.inboxify.nl/';

        $this->config->setConfig($config);
        
        $this->assertEquals(
            '/tmp/inboxify/',
            $this->config->getCacheDir()
        );

        $this->assertEquals(
            'https://api.inboxify.nl/',
            $this->config->getEndPoint()
        );
    }
}
